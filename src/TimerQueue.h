// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <vector>
#include <functional>
#include <unordered_map>

namespace qnet {

typedef std::function<void()> TimerCallback;

class TimerQueue
{
public:
    TimerQueue();
    ~TimerQueue();

    TimerQueue(const TimerQueue&) = delete;
    TimerQueue& operator=(const TimerQueue&) = delete;

    // Runs callback after `delay` milliseconds.
    int RunAfter(uint32_t delay, TimerCallback cb);

    // Runs callback every `interval` milliseconds.
    int RunEvery(uint32_t interval, TimerCallback cb);

    // Cancels the timer.
    bool Cancel(int timerid);

    int Run(int64_t timepoint);

private:
    struct TimerEntry
    {
        uint64_t        expires = 0;
        int             id = -1;
        int             index = -1;
        uint32_t        interval = 0;
        bool            repeat = false;
        TimerCallback   cb;

        bool Less(const TimerEntry& other)
        {
            return expires < other.expires;
        }
    };

    // Min-heap timer operation
    void siftup(int j);
    bool siftdown(int x, int n);

    void addEntry(TimerEntry& entry);
    void removeEntry(const TimerEntry& entry);
    void clear();

private:
    int next_id_ = 0;
    std::vector<TimerEntry> heap_;          // min-heap
    std::unordered_map<int, int> ref_;      // keep a reference to every timer, speed up searching
};

} // namespace qnet
