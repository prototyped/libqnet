// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <string>

namespace qnet {

class Clock
{
public:
    // Get CPU timestamp counter
    static int64_t GetNowTickCount();

    // Get current timestamp in milliseconds
    static int64_t GetCurrentTimeMillis();

    // Get current timestamp in milliseconds as string
    static std::string GetCurrentTimeString();
};

} // namespace qnet
