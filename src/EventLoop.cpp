// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "EventLoop.h"
#include "base/Logging.h"
#include "Clock.h"

#if defined(_WIN32)
#pragma comment(lib, "Ws2_32.lib")
#endif

namespace qnet {

const int POLL_TIMEOUT = 50;

EventLoop::EventLoop()
{
    // select(2) default
    const char* pollername = "select";

#if defined(_WIN32)
    WSADATA data;
    int r = WSAStartup(MAKEWORD(2, 2), &data);
    CHECK(r == 0);
#endif

#if defined(__linux__)
    pollername = "epoll";
#endif

    IPoller* poller = CreatePoller(pollername);
    CHECK(poller != nullptr) << pollername;
    poller_.reset(poller);
}

EventLoop::~EventLoop()
{
#if defined(_WIN32)
    WSACleanup();
#endif
}

IPoller* EventLoop::Poller()
{
    return poller_.get();
}


// Runs callback after `delay` milliseconds.
int EventLoop::RunAfter(uint32_t delay, TimerCallback cb)
{
    return timers_.RunAfter(delay, cb);
}

// Runs callback every `interval` milliseconds.
int EventLoop::RunEvery(uint32_t interval, TimerCallback cb)
{
    return timers_.RunEvery(interval, cb);
}

void EventLoop::Run()
{
    quit_ = false;
    while (!quit_)
    {
        Poll();
    }
}

void EventLoop::Poll()
{
    int64_t now = Clock::GetCurrentTimeMillis();
    cached_time_ = now;
    timers_.Run(now);

    poller_->Poll(POLL_TIMEOUT);

    std::vector<Runner> runners;
    runners.swap(pendingRunners_);
    for (auto fn : runners)
    {
        fn();
    }
    loops_++;
}

void EventLoop::Quit()
{
    quit_ = true;
}

void EventLoop::RunInLoop(Runner fn)
{
    pendingRunners_.push_back(fn);
}

} // namespace qnet
