// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <memory>
#include "Connector.h"
#include "TcpConnection.h"

namespace qnet {

// TCP client
class TcpClient
{
public:
    TcpClient(EventLoop* loop, const InetAddress& addr);
    ~TcpClient();

    TcpClient(const TcpClient&) = delete;
    TcpClient& operator=(const TcpClient&) = delete;

    void Connect();
    void Disconnect();
    void Stop();

    void SetConnectionCallback(ConnectionCallback cb)
    {
        on_connect_ = cb;
    }

    void SetMessageCallback(MessageCallback cb)
    {
        on_message_ = cb;
    }

    void setWriteCompleteCallback(WriteCompleteCallback cb)
    {
        on_write_complete_ = cb;
    }

private:
    void onNewConnection(SOCKET fd);
    void onRemoveConnection(const TcpConnectionPtr& conn);

private:
    EventLoop*  loop_ = nullptr;
    std::unique_ptr<Connector>  connector_;
    TcpConnectionPtr connection_;
    bool retry_ = false;
    bool connect_ = false;
    uint32_t next_sid_ = 0;

    ConnectionCallback      on_connect_;
    MessageCallback         on_message_;
    WriteCompleteCallback   on_write_complete_;
};

} // namespace qnet
