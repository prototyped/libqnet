// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include "TcpConnection.h"
#include "Acceptor.h"

namespace qnet {

// TCP server
class TcpServer
{
public:
    TcpServer(EventLoop* loop, const InetAddress& addr);
    ~TcpServer();

    TcpServer(const TcpServer&) = delete;
    TcpServer& operator=(const TcpServer&) = delete;

    int Start();

    void SetConnectionCallback(ConnectionCallback cb) { on_connected_ = cb; }
    void SetMessageCallback(MessageCallback cb) { on_message_ = cb; }
    void SetWriteCompleteCallback(WriteCompleteCallback cb) { on_write_complete_ = cb; }
    void SetCloseCallback(CloseCallback cb) { on_close_ = cb; }

    void RemoveConnection(const TcpConnectionPtr& conn);

private:
    void OnNewConnection(SOCKET fd, const InetAddress& addr);
    void OnConnectionMessage(const TcpConnectionPtr& conn, Buffer* buffer);
    void OnConnectionWriteComplete(const TcpConnectionPtr& conn);
    void OnConnectionClose(const TcpConnectionPtr& conn);

private:
    EventLoop* loop_ = nullptr;
    Acceptor  acceptor_;

    ConnectionCallback on_connected_;
    MessageCallback on_message_;
    WriteCompleteCallback on_write_complete_;
    CloseCallback on_close_;

    uint32_t next_id_ = 2018;   // next connection ID
    std::unordered_map<uint32_t, TcpConnectionPtr> connections_;

};

} // namespace qnet
