// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include "Platform.h"
#include <memory>
#include "base/StringPiece.h"
#include "EventLoop.h"
#include "InetAddress.h"
#include "Buffer.h"

namespace qnet {

class TcpConnection;
typedef std::shared_ptr<TcpConnection> TcpConnectionPtr;

typedef std::function<void(const TcpConnectionPtr&)> ConnectionCallback;
typedef std::function<void(const TcpConnectionPtr&)> CloseCallback;
typedef std::function<void(const TcpConnectionPtr&, Buffer*)> MessageCallback;
typedef std::function<void(const TcpConnectionPtr&)> WriteCompleteCallback;

// TCP connection object
class TcpConnection : public IPollEvents,
                      public std::enable_shared_from_this<TcpConnection>
{
public:
    TcpConnection(EventLoop* loop, uint32_t session, SOCKET fd, const InetAddress& localAddr, const InetAddress& remoteAddr);
    ~TcpConnection();

    TcpConnection(const TcpConnection&) = delete;
    TcpConnection& operator=(const TcpConnection&) = delete;

    uint32_t Session() const { return session_; }

    // event callbacks
    void SetMessageCallback(MessageCallback cb) { on_message_ = cb; }
    void SetWriteCompleteCallback(WriteCompleteCallback cb) { on_write_complete_ = cb; }
    void SetCloseCallback(CloseCallback cb) { on_close_ = cb; }

    // local and remote address
    const InetAddress& LocalAddress() const { return local_addr_; }
    const InetAddress& RemoteAddress() const { return remote_addr_; }

    void StartRead();
    void StopRead();

    void Send(const void* data, int len);
    void Send(StringPiece data);

    void Shutdown();

private:
    // I/O event
    void OnReadable() final;
    void OnWritable() final;

    void Clear();
    void HandleClose();
    void HandleError(int err);

private:
    EventLoop* loop_ = nullptr;
    InetAddress local_addr_;
    InetAddress remote_addr_;
    bool is_reading_ = false;
    SOCKET  fd_ = INVALID_FD;
    uint32_t session_ = 0;
    Buffer input_buf_;
    Buffer output_buf_;
    MessageCallback on_message_;
    WriteCompleteCallback on_write_complete_;
    CloseCallback on_close_;
};

} // namespace qnet
