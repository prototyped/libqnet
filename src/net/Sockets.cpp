// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Sockets.h"
#include <cassert>
#include "base/Logging.h"

namespace qnet {
namespace sockets {

int SetNonblock(SOCKET fd, bool nonblock)
{
#if defined(_WIN32)
    unsigned long val = nonblock ? 1 : 0;
    int r = ioctlsocket(fd, FIONBIO, &val);
    if (r == SOCKET_ERROR)
    {
        LOG(ERROR) << "ioctlsocket: " << GetLastError() << ": " << LAST_ERR_MSG;
        return -1;
    }
#else
    int flags = 0;

    /* Set the socket blocking (if non_block is zero) or non-blocking.
    * Note that fcntl(2) for F_GETFL and F_SETFL can't be
    * interrupted by a signal. */
    if ((flags = fcntl(fd, F_GETFL)) == -1) {
        LOG(ERROR) << "fcntl(F_GETFL): " << errno << ": " << strerror(errno);
        return -1;
    }

    if (nonblock)
        flags |= O_NONBLOCK;
    else
        flags &= ~O_NONBLOCK;

    if (fcntl(fd, F_SETFL, flags) == -1) {
        LOG(ERROR) << "fcntl(F_SETFL,O_NONBLOCK): " << errno << ": " << strerror(errno);
        return -1;
    }
#endif
    return 0;
}

int SetNoDelay(SOCKET fd, bool nodelay)
{
    int val = nodelay ? 1 : 0;
    int r = setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (const char*)&val, sizeof(val));
    if (r != 0)
    {
        LOG(ERROR) << "setsockopt TCP_NODELAY: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    return 0;
}

int Listen(SOCKET fd, const InetAddress& addr)
{
    socklen_t addrlen = 0;
    const sockaddr* paddr = addr.GetSockAddr(&addrlen);
    if (bind(fd, paddr, addrlen) != 0)
    {
        LOG(ERROR) << "bind: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    int yes = 1;
    int r = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes));
    if (r != 0)
    {
        LOG(ERROR) << "setsockopt SO_REUSEADDR: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    if (listen(fd, SOMAXCONN) != 0)
    {
        LOG(ERROR) << "listen: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    return 0;
}

SOCKET Accept(SOCKET acceptFd, InetAddress* paddr)
{
    sockaddr_storage addr = {};
    socklen_t addrlen = sizeof(addr);
    SOCKET fd = accept(acceptFd, reinterpret_cast<struct sockaddr*>(&addr), &addrlen);
    if (fd != INVALID_FD && paddr != nullptr)
    {
        paddr->CopySockAddr(addr);
    }
    return fd;
}

int Connect(SOCKET fd, const InetAddress& addr)
{
    socklen_t addrlen = 0;
    const sockaddr* paddr = addr.GetSockAddr(&addrlen);
    return connect(fd, paddr, addrlen);
}

int GetSocketError(SOCKET fd)
{
    int val = -1;
    socklen_t len = sizeof(val);
    if (getsockopt(fd, SOL_SOCKET, SO_ERROR, (char*)&val, &len) != 0)
    {
        LOG(ERROR) << "getsockopt SO_ERROR: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    return val;
}


int GetLocalAddr(SOCKET fd, InetAddress* paddr)
{
    assert(paddr != nullptr);
    sockaddr_storage addr;
    socklen_t addrlen = sizeof(addr);
    if (getsockname(fd, (struct sockaddr*)&addr, &addrlen) != 0)
    {
        LOG(ERROR) << "getsockname: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    paddr->CopySockAddr(addr);
    return 0;
}

int GetRemoteAddr(SOCKET fd, InetAddress* paddr)
{
    assert(paddr != nullptr);
    sockaddr_storage addr;
    socklen_t addrlen = sizeof(addr);
    if (getpeername(fd, (struct sockaddr*)&addr, &addrlen) != 0)
    {
        LOG(ERROR) << "getpeername: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
    paddr->CopySockAddr(addr);
    return 0;
}

bool IsSelfConnect(SOCKET fd)
{
    InetAddress localAddr, peerAddr;
    if (GetLocalAddr(fd, &localAddr) != 0)
    {
        return false;
    }
    if (GetRemoteAddr(fd, &peerAddr) != 0)
    {
        return false;
    }
    return localAddr.IsEqualTo(peerAddr);
}

} // namespace sockets
} // namespace qnet
