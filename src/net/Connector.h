// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include "Platform.h"
#include <functional>
#include "EventLoop.h"
#include "InetAddress.h"
#include "PollEvent.h"

namespace qnet {

// TCP connector
class Connector : public IPollEvents
{
public:
    typedef std::function<void(SOCKET)> NewConnectionCallback;

public:
    Connector(EventLoop* loop, const InetAddress& addr);
    ~Connector();

    Connector(const Connector&) = delete;
    Connector& operator=(const Connector&) = delete;

    void setNewConnectionCallback(NewConnectionCallback cb)
    {
        on_connected_ = cb;
    }

    void Start();
    void Stop();
    void Restart();

private:
    enum States 
    { 
        kDisconnected, 
        kConnecting, 
        kConnected,
    };
    enum
    {
        kMaxRetryDelayMs = 30 * 1000,
        kInitRetryDelayMs = 500,
    };

    int Connect();
    void Connecting(SOCKET fd);
    void Retry();
    void OnComplete();

    // I/O event handling
    void OnReadable() final;
    void OnWritable() final;

private:
    EventLoop* loop_ = nullptr;
    InetAddress addr_;
    SOCKET fd_ = INVALID_FD;
    bool started_ = true;
    States state_ = kDisconnected;
    NewConnectionCallback on_connected_;
    int retry_delay_ms_ = kInitRetryDelayMs;
};

} // namespace qnet
