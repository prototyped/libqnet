// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "TcpConnection.h"
#include "base/Logging.h"


namespace qnet {

TcpConnection::TcpConnection(EventLoop* loop, uint32_t session, SOCKET fd, const InetAddress& localAddr, const InetAddress& remoteAddr)
    : loop_(loop), local_addr_(localAddr), remote_addr_(remoteAddr), fd_(fd), session_(session)
{
    CHECK_NOTNULL(loop);
    loop_->Poller()->AddFd(fd_, this);
}

TcpConnection::~TcpConnection()
{
    Clear();
}

void TcpConnection::Clear()
{
    loop_->Poller()->RemoveFd(fd_);
    close(fd_);
    fd_ = INVALID_FD;
}



void TcpConnection::StartRead()
{
    if (!is_reading_)
    {
        is_reading_ = true;
        loop_->Poller()->SetPollIn(fd_);
    }
}

void TcpConnection::StopRead()
{
    if (is_reading_)
    {
        is_reading_ = false;
        loop_->Poller()->ResetPollIn(fd_);
    }
}

void TcpConnection::Send(const void* data, int len)
{
    int remain_bytes = len;
    int nwrote = send(fd_, (const char*)data, len, 0);
    if (nwrote >= 0)
    {
        remain_bytes = len - nwrote;
        if (remain_bytes == 0 && on_write_complete_)
        {
            loop_->RunInLoop(std::bind(on_write_complete_, shared_from_this()));
        }
    }
    else
    {
        int err = LAST_ERR;
        if (err != WSAEWOULDBLOCK)
        {
            LOG(ERROR) << "send: " << err << ": " << LAST_ERR_MSG;
            HandleError(err);
            return;
        }
    }
}

void TcpConnection::Send(StringPiece data)
{
    Send(data.data(), (int)data.size());
}

// recv data from fd
void TcpConnection::OnReadable()
{
    while (true)
    {
        char buf[MAX_RECV_BLOCK];
        int n = recv(fd_, buf, MAX_RECV_BLOCK, 0);
        if (n > 0)
        {
            input_buf_.append(buf, n);
            continue;
        }
        else if (n == 0)
        {
            HandleClose();
        }
        else
        {
            int err = LAST_ERR;
            if (err != WSAEWOULDBLOCK)
            {
                LOG(ERROR) << "recv: " << err << ": " << LAST_ERR_MSG;
                HandleError(err);
                return;
            }
        }
        break;
    }
    if (on_message_)
    {
        on_message_(shared_from_this(), &input_buf_);
    }
}

// write data to fd
void TcpConnection::OnWritable()
{
    int bytes = (int)output_buf_.readableBytes();
    while (bytes > 0)
    {
        int n = send(fd_, output_buf_.peek(), bytes, 0);
        if (n > 0)
        {
            output_buf_.retrieve(n);
            bytes -= n;
        }
        else
        {
            break;
        }
    }
}

void TcpConnection::HandleClose()
{
}

void TcpConnection::HandleError(int err)
{

}

void TcpConnection::Shutdown()
{

}

} // namespace qnet
