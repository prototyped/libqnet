// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "TcpServer.h"
#include <cassert>
#include "base/Logging.h"

using namespace std::placeholders;

namespace qnet {

TcpServer::TcpServer(EventLoop* loop, const InetAddress& addr)
    : loop_(loop), acceptor_(loop, addr)
{
    CHECK(loop != nullptr);
    connections_.rehash(128);
}

TcpServer::~TcpServer()
{

}

int TcpServer::Start()
{
    assert(loop_);
    acceptor_.SetConnectionCallback(std::bind(&TcpServer::OnNewConnection, this, _1, _2));
    return acceptor_.Listen();
}


void TcpServer::OnNewConnection(SOCKET fd, const InetAddress& remoteAddr)
{
    InetAddress localAddr;
    sockets::GetLocalAddr(fd, &localAddr);
    uint32_t sid = next_id_++;
    TcpConnectionPtr conn = std::make_shared<TcpConnection>(loop_, sid, fd, localAddr, remoteAddr);
    connections_[sid] = conn;
    conn->SetCloseCallback(std::bind(&TcpServer::OnConnectionClose, this, _1));
    conn->SetMessageCallback(std::bind(&TcpServer::OnConnectionMessage, this, _1, _2));
    conn->SetWriteCompleteCallback(std::bind(&TcpServer::OnConnectionWriteComplete, this, _1));
    conn->StartRead();
}

void TcpServer::RemoveConnection(const TcpConnectionPtr& conn)
{
    connections_.erase(conn->Session());
}

void TcpServer::OnConnectionMessage(const TcpConnectionPtr& conn, Buffer* buffer)
{
    assert(buffer != nullptr);
    if (on_message_)
    {
        on_message_(conn, buffer);
    }
}

void TcpServer::OnConnectionWriteComplete(const TcpConnectionPtr& conn)
{
    if (on_write_complete_)
    {
        on_write_complete_(conn);
    }
}

void TcpServer::OnConnectionClose(const TcpConnectionPtr& conn)
{
    connections_.erase(conn->Session());
    if (on_close_)
    {
        on_close_(conn);
    }
}

} // namespace qnet
