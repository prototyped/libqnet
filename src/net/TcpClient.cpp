// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "TcpClient.h"
#include "base/Logging.h"
#include "Sockets.h"

using namespace std::placeholders;

namespace qnet {

TcpClient::TcpClient(EventLoop* loop, const InetAddress& addr)
    : loop_(loop), connector_(new Connector(loop, addr))
{
    CHECK_NOTNULL(loop);
    connector_->setNewConnectionCallback(std::bind(&TcpClient::onNewConnection, this, _1));
}

TcpClient::~TcpClient()
{

}

void TcpClient::Connect()
{
    connect_ = true;
    connector_->Start();
}

void TcpClient::Disconnect()
{
    connect_ = false;
}

void TcpClient::Stop()
{
    connect_ = false;
    if (connection_)
    {
        connection_->Shutdown();
    }
}

void TcpClient::onNewConnection(SOCKET fd)
{
    InetAddress peerAddr, localAddr;
    sockets::GetRemoteAddr(fd, &peerAddr);
    sockets::GetLocalAddr(fd, &localAddr);
    ++next_sid_;
    TcpConnectionPtr conn(new TcpConnection(loop_, next_sid_, fd, localAddr, peerAddr));
    conn->SetMessageCallback(on_message_);
    conn->SetCloseCallback(std::bind(&TcpClient::onRemoveConnection, this, _1));
    connection_ = conn;
}

void TcpClient::onRemoveConnection(const TcpConnectionPtr& conn)
{
    assert(connection_ = conn);
    connection_.reset();
    connector_->Restart();
}

} // namespace qnet
