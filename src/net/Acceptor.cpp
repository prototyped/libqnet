// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Acceptor.h"
#include <cassert>
#include "Platform.h"
#include "base/Logging.h"

namespace qnet {

Acceptor::Acceptor(EventLoop* loop, const InetAddress& addr)
    : loop_(loop), addr_(addr)
{
    CHECK_NOTNULL(loop); 
    SOCKET fd = socket(addr_.Family(), SOCK_STREAM, IPPROTO_TCP);
    CHECK(fd != INVALID_FD) << "socket: " << LAST_ERR << ": " << LAST_ERR_MSG;
    sockets::SetNonblock(fd, true);
    fd_ = fd;
}

Acceptor::~Acceptor()
{
    if (fd_ != INVALID_FD)
    {
        loop_->Poller()->RemoveFd(fd_);
        close(fd_);
        fd_ = INVALID_FD;
    }
}

int Acceptor::Listen()
{
    int r = sockets::Listen(fd_, addr_);
    if (r != 0)
    {
        return -1;
    }
    if (loop_->Poller()->AddFd(fd_, this) != 0)
    {
        return -1;
    }
    loop_->Poller()->SetPollIn(fd_);
    return 0;
}

void Acceptor::OnWritable()
{
    // do nothing here
}

// should we handle EMFILE
// see https://jin-yang.github.io/reference/linux/libev.html#The_special_problem_of_accept_ing_wh
void Acceptor::OnReadable()
{
    int max = MAX_ACCEPT_PER_CALL;
    while (max--)
    {
        InetAddress addr;
        SOCKET fd = sockets::Accept(fd_, &addr);
        if (fd == INVALID_FD)
        {
            if (LAST_ERR != WSAEWOULDBLOCK)
            {
                LOG(ERROR) << "accept: " << LAST_ERR << ": " << LAST_ERR_MSG;
            }
            return;
        }

        sockets::SetNonblock(fd, true);
        //sockets::SetNoDelay(fd, true);
        if (on_new_connection_)
        {
            on_new_connection_(fd, addr);
        }
    }
}

} // namespace qnet
