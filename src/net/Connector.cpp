// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Connector.h"
#include <cassert>
#include <algorithm>
#include "base/Logging.h"
#include "Sockets.h"

namespace qnet {

Connector::Connector(EventLoop* loop, const InetAddress& addr)
    : loop_(loop), addr_(addr)
{
    CHECK_NOTNULL(loop);
}

Connector::~Connector()
{
    Stop();
}

void Connector::Start()
{
    assert(state_ == kDisconnected);
    if (started_)
    {
        Connect();
    }
}

void Connector::Stop()
{
    started_ = false;
    if (fd_ != INVALID_FD)
    {
        Retry(); // do cleanup
    }
}

void Connector::Restart()
{
    started_ = true;
    state_ = kDisconnected;
    retry_delay_ms_ = kInitRetryDelayMs;
    Connect();
}

// what will happen if we encoutered EMFILE(too many open files)?
int Connector::Connect()
{
    SOCKET fd = socket(addr_.Family(), SOCK_STREAM, IPPROTO_TCP);
    if (fd == INVALID_FD)
    {
        LOG(ERROR) << "socket: " << LAST_ERR << ": " << LAST_ERR_MSG;
        Retry();
        return 0;
    }
    
    int r = sockets::SetNonblock(fd, true);
    CHECK(r == 0) << LAST_ERR << ": " << LAST_ERR_MSG;

    r = sockets::Connect(fd, addr_);
    if (r != 0)
    {
        r = LAST_ERR;
    }
    switch (r)
    {
#ifdef _WIN32
    case WSAEWOULDBLOCK:
        Connecting(fd);
        break;

    case WSAETIMEDOUT:
    case WSAEINTR:
        fd_ = fd;
        Retry();
        break;
#else
    case 0:
    case EINPROGRESS:
    case EINTR:
    case EISCONN:
        Connecting(fd);
        break;

    case EAGAIN:
    case EADDRINUSE:
    case EADDRNOTAVAIL:
    case ECONNREFUSED:
    case ENETUNREACH:
        fd_ = fd;
        Retry();
        break;
#endif
    default:
        LOG(ERROR) << LAST_ERR << ": " << LAST_ERR_MSG;
        close(fd);
        break;
    }

    return 0;
}

// a nonblocking connection attempt failed
void Connector::OnReadable()
{
    OnComplete();
}

void Connector::OnWritable()
{
    OnComplete();
}

void Connector::OnComplete()
{
    if (state_ != kConnecting)
    {
        // we may encountered error here
        assert(false);
        return;
    }
    int err = sockets::GetSocketError(fd_);
    if (err != 0)
    {
        LOG(ERROR) << "connect with SO_ERROR: " << err << ", " << GetErrorMessage(err);
        // try reconnect
        Retry();
    }
    else
    {
        if (sockets::IsSelfConnect(fd_))
        {
            Retry();
            return;
        }
        state_ = kConnected;
        if (on_connected_)
        {
            on_connected_(fd_);
        }
        else
        {
            close(fd_);
            fd_ = INVALID_FD;
        }
    }
}

void Connector::Connecting(SOCKET fd)
{
    assert(fd_ == INVALID_FD);
    state_ = kConnecting;
    loop_->Poller()->AddFd(fd, this); // Should we check reach the select 1024 limit?
    loop_->Poller()->SetPollOut(fd);
    fd_ = fd;
}

void Connector::Retry()
{
    loop_->Poller()->RemoveFd(fd_);
    close(fd_);
    fd_ = INVALID_FD;
    state_ = kDisconnected;
    if (started_)
    {
        loop_->RunAfter(retry_delay_ms_, std::bind(&Connector::Start, this));
        retry_delay_ms_ = std::min(retry_delay_ms_ * 2, (int)kMaxRetryDelayMs);
    }
}

} // namespace qnet
