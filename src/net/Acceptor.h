// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include "PollEvent.h"
#include "Sockets.h"
#include "EventLoop.h"

namespace qnet {

// TCP acceptor
class Acceptor : public IPollEvents
{
public:
    typedef std::function<void(SOCKET fd, const InetAddress&)> NewConnectionCallback;

public:
    Acceptor(EventLoop* loop, const InetAddress& addr);
    ~Acceptor();

    Acceptor(const Acceptor&) = delete;
    Acceptor& operator=(const Acceptor&) = delete;

    int Listen();

    void SetConnectionCallback(NewConnectionCallback cb)
    {
        on_new_connection_ = cb;
    }

private:
    // I/O event handling
    void OnReadable() final;
    void OnWritable() final;

private:
    EventLoop* loop_ = nullptr;
    InetAddress addr_;
    SOCKET fd_ = INVALID_FD;
    NewConnectionCallback on_new_connection_;
};

} // namespace qnet
