// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include "Platform.h"
#include "InetAddress.h"

namespace qnet {
namespace sockets {

// Set socket in noblocking mode or not
int SetNonblock(SOCKET fd, bool nonblock);

int SetNoDelay(SOCKET fd, bool nodelay);

// Bind and listen
int Listen(SOCKET fd, const InetAddress& addr);

// Accept an new socket
SOCKET Accept(SOCKET acceptFd, InetAddress* paddr);

// Connect to an remote address
int Connect(SOCKET fd, const InetAddress& addr);

// Get error code socket, SO_ERROR
int GetSocketError(SOCKET fd);

int GetLocalAddr(SOCKET fd, InetAddress* addr);

int GetRemoteAddr(SOCKET fd, InetAddress* addr);

bool IsSelfConnect(SOCKET fd);

} // namespace sockets
} // namespace qnet
