// Copyright 2010, Shuo Chen.  All rights reserved.
// http://code.google.com/p/muduo/
//
// Use of this source code is governed by a BSD-style license
// that can be found in the License file.

// Author: Shuo Chen (chenshuo at chenshuo dot com)

#include "InetAddress.h"
#include <cstring>
#include <cassert>
#include "base/Logging.h"
#include "base/Endian.h"
#include "base/Strings.h"


//     /* Structure describing an Internet socket address.  */
//     struct sockaddr_in {
//         sa_family_t    sin_family; /* address family: AF_INET */
//         uint16_t       sin_port;   /* port in network byte order */
//         struct in_addr sin_addr;   /* internet address */
//     };

//     /* Internet address. */
//     typedef uint32_t in_addr_t;
//     struct in_addr {
//         in_addr_t       s_addr;     /* address in network byte order */
//     };

//     struct sockaddr_in6 {
//         sa_family_t     sin6_family;   /* address family: AF_INET6 */
//         uint16_t        sin6_port;     /* port in network byte order */
//         uint32_t        sin6_flowinfo; /* IPv6 flow information */
//         struct in6_addr sin6_addr;     /* IPv6 address */
//         uint32_t        sin6_scope_id; /* IPv6 scope-id */
//     };


InetAddress::InetAddress(uint16_t port, bool loopbackOnly, bool ipv6)
{
    if (ipv6)
    {
        memset(&addr6_, 0, sizeof(addr6_));
        addr6_.sin6_family = AF_INET6;
        in6_addr ip = loopbackOnly ? in6addr_loopback : in6addr_any;
        addr6_.sin6_addr = ip;
        addr6_.sin6_port = Endian::big(port);
    }
    else
    {
        memset(&addr_, 0, sizeof(addr_));
        addr_.sin_family = AF_INET;
        uint32_t ip = loopbackOnly ? INADDR_LOOPBACK : INADDR_ANY;
        addr_.sin_addr.s_addr = Endian::big(ip);
        addr_.sin_port = Endian::big(port);
    }
}

InetAddress::InetAddress(StringPiece ip, uint16_t port, bool ipv6)
{
    int r = 0;
    if (ipv6)
    {
        memset(&addr6_, 0, sizeof(addr6_));
        addr6_.sin6_family = AF_INET6;
        addr6_.sin6_port = Endian::big(port);
        r = inet_pton(AF_INET6, ip.data(), &addr6_.sin6_addr);
    }
    else
    {
        memset(&addr_, 0, sizeof(addr_));
        addr_.sin_family = AF_INET;
        addr_.sin_port = Endian::big(port);
        r = inet_pton(AF_INET, ip.data(), &addr_.sin_addr);
    }
    CHECK(r == 1) << "inet_pton: " << LAST_ERR << ": " << LAST_ERR_MSG;
}

uint32_t InetAddress::IP() const
{
    assert(addr_.sin_family == AF_INET);
    return addr_.sin_addr.s_addr;
}

uint16_t InetAddress::Port() const
{
    return Endian::big(addr_.sin_port);
}

std::string InetAddress::IpPortString() const
{
    std::string ip = IpString();
    uint16_t port = Endian::big(addr_.sin_port);
    return StringPrintf("%s:%u", ip.c_str(), port);
}

std::string InetAddress::IpString() const
{
    char buf[64] = "";
    const char* r = nullptr;
    if (addr_.sin_family == AF_INET)
    {
        r = inet_ntop(AF_INET, (void*)&addr_.sin_addr, buf, 64);
    }
    else if (addr_.sin_family == AF_INET6)
    {
        r = inet_ntop(AF_INET6, (void*)&addr6_.sin6_addr, buf, 64);
    }
    CHECK(r != nullptr) << "inet_ntop: " << LAST_ERR << ": " << LAST_ERR_MSG;
    return buf;
}

const struct sockaddr* InetAddress::GetSockAddr(socklen_t* paddrlen) const
{
    switch (addr_.sin_family)
    {
    case AF_INET:
        if (paddrlen != nullptr)
        {
            *paddrlen = sizeof(addr_);
        }
        break;

    case AF_INET6:
        if (paddrlen != nullptr)
        {
            *paddrlen = sizeof(addr6_);
        }
        break;
    }
    return reinterpret_cast<const struct sockaddr*>(&addr6_);
}

void InetAddress::CopySockAddr(const struct sockaddr_storage& addr)
{
    memcpy(&addr6_, &addr, sizeof(addr6_));
}

bool InetAddress::IsEqualTo(const InetAddress& rhs) const 
{
    if (addr_.sin_family == rhs.addr_.sin_family)
    {
        switch (addr_.sin_family)
        {
        case AF_INET:
            return (addr_.sin_port == rhs.addr_.sin_port) && 
                addr_.sin_addr.s_addr == rhs.addr_.sin_addr.s_addr;
        case AF_INET6:
            return (addr6_.sin6_port == rhs.addr6_.sin6_port) && 
                memcmp(&addr6_.sin6_addr, &rhs.addr6_.sin6_addr, sizeof(addr6_.sin6_addr)) == 0;
        }
    }
    return false;
}

int InetAddress::Resolve(StringPiece host, StringPiece port, std::vector<InetAddress>& result)
{
    struct addrinfo* ailist = nullptr;
    struct addrinfo hint = {};
    hint.ai_family = AF_UNSPEC;
    int r = getaddrinfo(host.data(), port.data(), &hint, &ailist);
    if (r != 0)
    {
        LOG(ERROR) << StringPrintf("getaddrinfo: %s:%s, %s", host.data(), port.data(),gai_strerror(r));
        return r;
    }
    for (addrinfo* pinfo = ailist; pinfo != nullptr; pinfo = pinfo->ai_next)
    {
        switch (pinfo->ai_family)
        {
        case AF_INET:
            {
                auto paddr = reinterpret_cast<const struct sockaddr_in*>(pinfo->ai_addr);
                result.push_back(InetAddress(*paddr));
            }
            break;

        case AF_INET6:
            {
                auto paddr = reinterpret_cast<const struct sockaddr_in6*>(pinfo->ai_addr);
                result.push_back(InetAddress(*paddr));
            }
            break;

        default:
            continue;
        }
    }
    freeaddrinfo(ailist);
    return 0;
}
