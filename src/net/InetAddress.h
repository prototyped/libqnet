// Copyright 2010, Shuo Chen.  All rights reserved.
// http://code.google.com/p/muduo/
//
// Use of this source code is governed by a BSD-style license
// that can be found in the License file.

// Author: Shuo Chen (chenshuo at chenshuo dot com)
//
// This is a public header file, it must only include public header files.

#pragma once

#include "Platform.h"
#include <cstdint>
#include <string>
#include <vector>
#include "base/StringPiece.h"


///
/// Wrapper of sockaddr_in.
///
/// This is an POD interface class.
class InetAddress
{
public:
    /// Constructs an endpoint with given port number.
    /// Mostly used in TcpServer listening.
    explicit InetAddress(uint16_t port = 0, bool loopbackOnly = false, bool ipv6 = false);

    /// Constructs an endpoint with given ip and port.
    /// @c ip should be "1.2.3.4"
    InetAddress(StringPiece ip, uint16_t port, bool ipv6 = false);

    /// Constructs an endpoint with given struct @c sockaddr_in
    /// Mostly used when accepting new connections
    explicit InetAddress(const struct sockaddr_in& addr)
        : addr_(addr)
    { }

    explicit InetAddress(const struct sockaddr_in6& addr)
        : addr6_(addr)
    { }

    uint16_t Family() const { return addr_.sin_family; }
    uint32_t IP() const;
    uint16_t Port() const;

    std::string IpString() const;
    std::string IpPortString() const;

    const struct sockaddr* GetSockAddr(socklen_t* paddrlen) const;
    void CopySockAddr(const struct sockaddr_storage& addr);

    bool IsEqualTo(const InetAddress& rhs) const;

    // resolve hostname to IP address, not changing port or sin_family
    // return 0 on success.
    static int Resolve(StringPiece host, StringPiece port, std::vector<InetAddress>& result);

private:
    union
    {
        struct sockaddr_in addr_;
        struct sockaddr_in6 addr6_;
    };
};
