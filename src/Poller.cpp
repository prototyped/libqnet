// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.


#include "Poller.h"
#include "base/Logging.h"
#include "Clock.h"
#include "poller/PollerEpoll.h"
#include "poller/PollerSelect.h"

namespace qnet {

IPoller* CreatePoller(const std::string& name)
{
    if (name == "select")
    {
        return new SelectPoller();
    }
#ifdef POLLER_HAS_EPOLL
    else if (name == "epoll")
    {
        return new EpollPoller();
    }
#endif
    return nullptr;
}

} // namespace qnet
