// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

namespace qnet {

// Interface of object which want to be notified on I/O events
struct IPollEvents
{
    virtual ~IPollEvents() {}

    // Called when file descriptor is ready for reading
    virtual void OnReadable() = 0;

    // Called when file descriptor is ready for writing
    virtual void OnWritable() = 0;
};

} // namespace qnet
