// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <vector>
#include <memory>
#include <functional>
#include "TimerQueue.h"
#include "Poller.h"

namespace qnet {

typedef std::function<void()> Runner;

class EventLoop
{
public:
    EventLoop();
    ~EventLoop();

    EventLoop(const EventLoop&) = delete;
    EventLoop& operator=(const EventLoop&) = delete;

    // Runs callback after `delay` milliseconds.
    int RunAfter(uint32_t delay, TimerCallback cb);

    // Runs callback every `interval` milliseconds.
    int RunEvery(uint32_t interval, TimerCallback cb);

    void Run();

    void Poll();

    void Quit();

    void RunInLoop(Runner fn);

    IPoller* Poller();

private:
    std::unique_ptr<IPoller> poller_;
    bool quit_ = false;

    int64_t loops_ = 0;
    int64_t cached_time_ = 0;
    TimerQueue timers_;

    std::vector<Runner> pendingRunners_;
};

} // namespace qnet
