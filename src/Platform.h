// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cerrno>

// detection for 64 bit
#if defined(__x86_64__) || defined(_M_X64)
# define PLATFORM_X64  1
#else
# define PLATFORM_X64  0
#endif

const char* GetErrorMessage(int);

#define POLLER_HAS_SELECT 

#if defined(_WIN32)

# if defined(_USE_32BIT_TIME_T)
# undef _USE_32BIT_TIME_T
# endif

#include <WinSock2.h>
#include <WS2tcpip.h>


typedef int socklen_t;

#define close           closesocket
#define INVALID_FD      INVALID_SOCKET
#define LAST_ERR        (GetLastError())
#define LAST_ERR_MSG    GetErrorMessage(GetLastError())


#elif defined(__linux__)

#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

typedef int SOCKET;

#define INVALID_FD      (-1)
#define LAST_ERR        errno
#define LAST_ERR_MSG    strerror(errno)
#define WSAEWOULDBLOCK  EAGAIN

#define POLLER_HAS_EPOLL

#else
# error "unsupported platform"
#endif

#if defined(_MSC_VER) && _MSC_VER <= 1800
#define snprintf _snprintf
#endif

// Maximum number of events the I/O thread can process in one go.
#define MAX_IO_EVENT    256

// Maximum bytes of recv block in one shot
#define MAX_RECV_BLOCK  4096

#define MAX_ACCEPT_PER_CALL 256
