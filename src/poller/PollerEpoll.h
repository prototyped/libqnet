// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include "Platform.h"
#include <sys/epoll.h>
#include <vector>
#include "PollEvent.h"
#include "Poller.h"

#ifdef POLLER_HAS_EPOLL

namespace qnet {

// I/O Multiplexing with epoll(2).
class EpollPoller : public IPoller
{
public:
    EpollPoller();
    ~EpollPoller();

    int AddFd(SOCKET fd, IPollEvents* sink);
    void RemoveFd(SOCKET fd);

    void SetPollIn(SOCKET fd);
    void ResetPollIn(SOCKET fd);
    void SetPollOut(SOCKET fd);
    void ResetPollOut(SOCKET fd);

    int Poll(int timeout);

private:
    struct PollEntry
    {
        int fd;
        epoll_event ev;
        IPollEvents* sink;
    };

    std::vector<PollEntry*> retired_fds_;
    int epoll_fd_ = -1;
};

} // namespace qnet

#endif // POLLER_HAS_EPOLL
