// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.


#if defined(__linux__)

#include "PollerEpoll.h"
#include <cstring>
#include "base/Logging.h"

namespace qnet {

EpollPoller::EpollPoller()
{
    epoll_fd_ = epoll_create1(0);
    CHECK(epoll_fd_ != -1) "epoll_create: " << errno << ": " << strerror(errno);
}

EpollPoller::~EpollPoller()
{
    if (epoll_fd_ != -1)
    {
        close(epoll_fd_);
        epoll_fd_ = -1;
    }
}

int EpollPoller::AddFd(SOCKET fd, IPollEvents* sink)
{
    CHECK(sink != NULL);
    PollEntry* pe = new PollEntry();
    pe->fd = fd;
    pe->ev.events = 0;
    pe->ev.data.ptr = pe;
    pe->sink = sink;

    int r = epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, fd, &pe->ev);
    if (r != 0)
    {
        LOG(ERROR) << "epoll_ctl add: " << errno << ": " << strerror(errno);
        return -1;
    }

    return 0;
}

void EpollPoller::RemoveFd(SOCKET fd)
{
    PollEntry* pe = reinterpret_cast<PollEntry*>(fd);
    int r = epoll_ctl(epoll_fd_, EPOLL_CTL_DEL, pe->fd, &pe->ev);
    if (r != 0)
    {
        LOG(ERROR) << "epoll_ctl del: " << errno << ": " << strerror(errno);
        return;
    }
    retired_fds_.push_back(pe);
}

void EpollPoller::SetPollIn(SOCKET fd)
{
    PollEntry* pe = reinterpret_cast<PollEntry*>(fd);
    pe->ev.events |= EPOLLIN;
    int r = epoll_ctl(epoll_fd_, EPOLL_CTL_MOD, pe->fd, &pe->ev);
    if (r != 0)
    {
        LOG(ERROR) << "epoll_ctl mod: " << errno << ": " << strerror(errno);
    }
}

void EpollPoller::ResetPollIn(SOCKET fd)
{
    PollEntry* pe = reinterpret_cast<PollEntry*>(fd);
    pe->ev.events &= ~((short)EPOLLIN);
    int r = epoll_ctl(epoll_fd_, EPOLL_CTL_MOD, pe->fd, &pe->ev);
    if (r != 0)
    {
        LOG(ERROR) << "epoll_ctl mod: " << errno << ": " << strerror(errno);
    }
}

void EpollPoller::SetPollOut(SOCKET fd)
{
    PollEntry* pe = reinterpret_cast<PollEntry*>(fd);
    pe->ev.events |= EPOLLOUT;
    int r = epoll_ctl(epoll_fd_, EPOLL_CTL_MOD, pe->fd, &pe->ev);
    if (r != 0)
    {
        LOG(ERROR) << "epoll_ctl mod: " << errno << ": " << strerror(errno);
    }
}

void EpollPoller::ResetPollOut(SOCKET fd)
{
    PollEntry* pe = reinterpret_cast<PollEntry*>(fd);
    pe->ev.events &= ~((short)EPOLLOUT);
    int r = epoll_ctl(epoll_fd_, EPOLL_CTL_MOD, pe->fd, &pe->ev);
    if (r != 0)
    {
        LOG(ERROR) << "epoll_ctl mod: " << errno << ": " << strerror(errno);
    }
}

int EpollPoller::Poll(int timeout)
{
    epoll_event events[MAX_IO_EVENT] = {};
    int r = epoll_wait(epoll_fd_, &events[0], MAX_IO_EVENT, timeout > 0 ? timeout : -1);
    if (r == -1)
    {
        LOG(ERROR) << "epoll_wait: " << errno << ": " << strerror(errno);
        return -1;
    }
    int fired = 0;
    for (int i = 0; i < r; i++)
    {
        PollEntry* pe = reinterpret_cast<PollEntry*>(events[i].data.ptr);
        if (pe->fd == -1)
        {
            continue;
        }
        if (events[i].events & (EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP))
        {
            pe->sink->OnReadable();
            fired++;
        }
        if (pe->fd == -1)
        {
            continue;
        }
        if (events[i].events & (EPOLLOUT | EPOLLHUP | EPOLLERR))
        {
            pe->sink->OnWritable();
            fired++;
        }
    }
    //  Destroy retired event sources
    for (size_t i = 0; i < retired_fds_.size(); i++)
    {
        delete retired_fds_[i];
    }
    retired_fds_.clear();
    return fired;
}

} // namespace qnet

#endif // __linux__
