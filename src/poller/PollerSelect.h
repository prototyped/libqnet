// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License. 
// See accompanying files LICENSE.

#pragma once

#include "Platform.h"
#include <vector>
#include "PollEvent.h"
#include "Poller.h"

#ifdef POLLER_HAS_SELECT

namespace qnet {

// I/O Multiplexing with select(2).
class SelectPoller : public IPoller
{
public:
    SelectPoller();
    ~SelectPoller();

    int AddFd(SOCKET fd, IPollEvents* sink);
    void RemoveFd(SOCKET fd);

    void SetPollIn(SOCKET fd);
    void ResetPollIn(SOCKET fd);
    void SetPollOut(SOCKET fd);
    void ResetPollOut(SOCKET fd);

    int Poll(int timeout);

private:
    struct PollEntry
    {
        SOCKET          fd;
        IPollEvents*    sink;
    };

    std::vector<PollEntry> fds_;
    SOCKET maxfd_ = INVALID_FD;
    bool has_retired_ = false;

    fd_set      readfds_;
    fd_set      writefds_;
    fd_set      exceptfds_;
};

} // namespace qnet

#endif // POLLER_HAS_SELECT
