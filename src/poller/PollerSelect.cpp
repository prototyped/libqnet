// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "PollerSelect.h"
#include <cstring>
#include <algorithm>
#include "base/Logging.h"

#ifdef POLLER_HAS_SELECT

namespace qnet {

SelectPoller::SelectPoller()
{
    fds_.reserve(64);
    memset(&readfds_, 0, sizeof(fd_set));
    memset(&writefds_, 0, sizeof(fd_set));
    memset(&exceptfds_, 0, sizeof(fd_set));
}

SelectPoller::~SelectPoller()
{
}

int SelectPoller::AddFd(SOCKET fd, IPollEvents* sink)
{
    CHECK(sink != nullptr);
    if (fds_.size() >= FD_SETSIZE)
    {
        LOG(ERROR) << "select fd_set size overflow";
        return -1;
    }
    PollEntry entry = { fd, sink };
    fds_.push_back(entry);

    // start polling on errors
    FD_SET(fd, &exceptfds_);

    if (fd > maxfd_)
    {
        maxfd_ = fd;
    }
    return 0;
}

void SelectPoller::RemoveFd(SOCKET fd)
{
    // mark the descriptor as retired
    for (size_t i = 0; i < fds_.size(); i++)
    {
        if (fds_[i].fd == fd)
        {
            fds_[i].fd = INVALID_FD;
            break;
        }
    }
    has_retired_ = true;

    // Discard all events generated on this file descriptor.
    FD_CLR(fd, &readfds_);
    FD_CLR(fd, &writefds_);
    FD_CLR(fd, &exceptfds_);

    //  Adjust the maxfd attribute if we have removed the
    //  highest-numbered file descriptor.
    if (fd == maxfd_)
    {
        maxfd_ = INVALID_FD;
        for (size_t i = 0; i < fds_.size(); i++)
        {
            if (fds_[i].fd > maxfd_)
            {
                maxfd_ = fds_[i].fd;
            }
        }
    }
}

void SelectPoller::SetPollIn(SOCKET fd)
{
    FD_SET(fd, &readfds_);
}

void SelectPoller::ResetPollIn(SOCKET fd)
{
    FD_CLR(fd, &readfds_);
}

void SelectPoller::SetPollOut(SOCKET fd)
{
    FD_SET(fd, &writefds_);
}

void SelectPoller::ResetPollOut(SOCKET fd)
{
    FD_CLR(fd, &writefds_);
}

int SelectPoller::Poll(int timeout)
{
    if (fds_.empty())
    {
        return 0;
    }

    // Initialize the poolsets
    fd_set rdset, wrset, exptset;
    memcpy(&rdset, &readfds_, sizeof(fd_set));
    memcpy(&wrset, &writefds_, sizeof(fd_set));
    memcpy(&exptset, &exceptfds_, sizeof(fd_set));

    // wait for events
    timeval tvp = {};
    tvp.tv_usec = timeout * 1000;
#if defined(_WIN32)
    int r = select(0, &rdset, &wrset, &exptset, &tvp);
    if (r == SOCKET_ERROR)
    {
        LOG(ERROR) << "select: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
#else
    int r = select(maxfd_ + 1, &rdset, &wrset, &exptset, &tvp);
    if (r == -1)
    {
        LOG(ERROR) << "select: " << LAST_ERR << ": " << LAST_ERR_MSG;
        return -1;
    }
#endif
    int fired = 0;
    if (r == 0)
        return 0;

    for (size_t i = 0; i < fds_.size(); i++)
    {
        SOCKET fd = fds_[i].fd;
        if (fd == INVALID_FD)
            continue;

        if (FD_ISSET(fd, &exptset))
        {
            fds_[i].sink->OnReadable();
            fired++;
        }
        if (fds_[i].fd == INVALID_FD)
            continue;

        if (FD_ISSET(fd, &wrset))
        {
            fds_[i].sink->OnWritable();
            fired++;
        }
        if (fds_[i].fd == INVALID_FD)
            continue;

        if (FD_ISSET(fd, &rdset))
        {
            fds_[i].sink->OnReadable();
            fired++;
        }
    }

    if (has_retired_)
    {
        auto iter = std::remove_if(fds_.begin(), fds_.end(), [](const PollEntry& entry)
        {
            return entry.fd == INVALID_FD;
        });
        fds_.erase(iter, fds_.end());
        has_retired_ = false;
    }

    return r;
}

} // namespace qnet

#endif // POLLER_HAS_SELECT
