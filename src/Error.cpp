// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Platform.h"

const char* GetErrorMessage(int err)
{
    static char msg[512]; // this is thread-local in C++ 11
#if defined(_WIN32)
    FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err,
        0, msg, 511, NULL);
    return msg;
#else
    return strerror(err);
#endif
}
