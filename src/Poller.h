// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <string>
#include "Platform.h"
#include "PollEvent.h"

namespace qnet {

struct IPoller
{
    virtual int AddFd(SOCKET fd, IPollEvents* sink) = 0;
    virtual void RemoveFd(SOCKET fd) = 0;

    virtual void SetPollIn(SOCKET fd) = 0;
    virtual void ResetPollIn(SOCKET fd) = 0;
    virtual void SetPollOut(SOCKET fd) = 0;
    virtual void ResetPollOut(SOCKET fd) = 0;

    virtual int Poll(int timeout) = 0;
};

IPoller* CreatePoller(const std::string& name);

} // namespace qnet
