// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "TimerQueue.h"
#include "base/Logging.h"
#include "Clock.h"

namespace qnet {

TimerQueue::TimerQueue()
{
    heap_.reserve(64);
}

TimerQueue::~TimerQueue()
{
    clear();
}

// Runs callback after `delay` milliseconds.
int TimerQueue::RunAfter(uint32_t delay, TimerCallback cb)
{
    CHECK(cb != nullptr);
    uint64_t expires = Clock::GetCurrentTimeMillis() + delay;

    TimerEntry entry;
    entry.repeat = false;
    entry.id = next_id_;
    entry.expires = expires;
    entry.cb = cb;
    entry.interval = delay;

    addEntry(entry);
    next_id_++;
    return entry.id;
}

// Runs callback every `interval` milliseconds.
int TimerQueue::RunEvery(uint32_t interval, TimerCallback cb)
{
    CHECK(cb != nullptr);
    uint64_t expires = Clock::GetCurrentTimeMillis() + interval;

    TimerEntry entry;
    entry.interval = interval;
    entry.repeat = true;
    entry.id = next_id_;
    entry.expires = expires;
    entry.cb = cb;

    addEntry(entry);
    next_id_++;
    return entry.id;
}

// This operation takes O(n) complexity
bool TimerQueue::Cancel(int timerid)
{
    auto iter = ref_.find(timerid);
    if (iter != ref_.end())
    {
        removeEntry(heap_[iter->second]);
        return true;
    }
    return false;
}

void TimerQueue::removeEntry(const TimerEntry& entry)
{
    int n = (int)heap_.size() - 1;
    int i = entry.index;
    if (i != n)
    {
        std::swap(heap_[i], heap_[n]);
        heap_[i].index = i;
        if (!siftdown(i, n))
        {
            siftup(i);
        }
    }
    heap_.pop_back();
    ref_.erase(entry.id);
}

void TimerQueue::addEntry(TimerEntry& entry)
{
    entry.index = (int)heap_.size();
    heap_.emplace_back(entry);
    siftup((int)heap_.size() - 1);
    ref_[entry.id] = entry.index;
}

// move a timer node up in the tree, as long as needed;
void TimerQueue::siftup(int j)
{
    for (;;)
    {
        int i = (j - 1) / 2; // parent node
        if (i == j || !heap_[j].Less(heap_[i]))
        {
            break;
        }
        std::swap(heap_[i], heap_[j]);
        heap_[i].index = i;
        heap_[j].index = j;
        j = i;
    }
}

// move a timer node down in the tree, similar to sift-up
bool TimerQueue::siftdown(int x, int n)
{
    int i = x;
    for (;;)
    {
        int j1 = 2 * i + 1;
        if ((j1 >= n) || (j1 < 0)) // j1 < 0 after int overflow
        {
            break;
        }
        int j = j1; // left child
        int j2 = j1 + 1;
        if (j2 < n && !heap_[j1].Less(heap_[j2]))
        {
            j = j2; // right child
        }
        if (!heap_[j].Less(heap_[i]))
        {
            break;
        }
        std::swap(heap_[i], heap_[j]);
        heap_[i].index = i;
        heap_[j].index = j;
        i = j;
    }
    return i > x;
}

int TimerQueue::Run(int64_t now)
{
    // fast case
    if (heap_.empty())
    {
        return 0;
    }
    int fired = 0;
    while (!heap_.empty())
    {
        TimerEntry entry = heap_[0];
        if (now < entry.expires)
        {
            break;
        }

        int n = (int)heap_.size() - 1;
        std::swap(heap_[0], heap_[n]);
        heap_[0].index = 0;
        siftdown(0, n);
        heap_.pop_back();
        
        fired++;

        if (entry.cb != nullptr)
        {
            entry.cb();
        }
        if (entry.repeat)
        {
            entry.expires += entry.interval;
            addEntry(entry);
        }
        else
        {
            ref_.erase(entry.id);
        }
    }
    return fired;
}

void TimerQueue::clear()
{
    heap_.clear();
    ref_.clear();
}

} // namespace qnet
