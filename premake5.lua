--
-- Premake script (http://premake.github.io)
--

solution 'libqnet'
    configurations  {'Debug', 'Release'}
    architecture 'x64'
    targetdir    'bin'

    filter 'configurations:Debug'
        defines { 'DEBUG' }
        symbols 'On'

    filter 'configurations:Release'
        defines { 'NDEBUG' }
        symbols 'On'
        optimize 'On'

    filter 'action:vs*'
        defines
        {
            'WIN32',
            'WIN32_LEAN_AND_MEAN',
            '_WIN32_WINNT=0x0600',
            '_CRT_SECURE_NO_WARNINGS',
            '_SCL_SECURE_NO_WARNINGS',
            'NOMINMAX',
        }
    filter "action:gmake"
        buildoptions '-std=c++11'
        links 'pthread'


    project 'libqnet'
        language    'C++'
        kind        'StaticLib'
        defines
        {
        }
        includedirs
        {
            'src',
        }

        files
        {
            'src/**.cpp',
            'src/**.h'
        }

    project 'unittest'
        language    'C++'
        kind        'ConsoleApp'
        defines
        {
        }
        includedirs
        {
            'src',
            '3rdparty/gtest',
            '3rdparty/gtest/include',
        }

        files
        {
            '3rdparty/gtest/src/gtest-all.cc',
            'test/**.h',
            'test/**.cpp',
        }
        links 'libqnet'

    project 'echo'
        language    'C++'
        kind        'ConsoleApp'
        defines
        {
        }
        includedirs
        {
            'src',
            'examples/echo',
        }

        files
        {
            'examples/echo/**.h',
            'examples/echo/**.cpp',
        }
        links 'libqnet'

    project 'pingpong'
        language    'C++'
        kind        'ConsoleApp'
        defines
        {
        }
        includedirs
        {
            'src',
            'examples/pingpong',
        }

        files
        {
            'examples/pingpong/**.h',
            'examples/pingpong/**.cpp',
        }
        links 'libqnet'