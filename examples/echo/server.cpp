// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License. 
// See accompanying files LICENSE.

#include <iostream>
#include <EventLoop.h>
#include <net/InetAddress.h>
#include <net/TcpServer.h>

using namespace std;
using namespace qnet;


static void onNewConnection(const TcpConnectionPtr& conn)
{
    cout << conn->Session() << endl;
}

void StartServer(uint16_t port)
{
    EventLoop loop;
    InetAddress addr(port);
    TcpServer server(&loop, addr);
    server.SetConnectionCallback(onNewConnection);
    server.Start();
    loop.Run();
}
