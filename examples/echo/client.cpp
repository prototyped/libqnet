// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License. 
// See accompanying files LICENSE.

#include <string>
#include <iostream>
#include "EventLoop.h"
#include "net/TcpClient.h"

using namespace std;
using namespace qnet;

static void onNewConnection(const TcpConnectionPtr& conn)
{
    cout << conn->Session() << endl;
}

void StartClient(std::string host, uint16_t port)
{
    EventLoop loop;
    InetAddress addr(host, port);
    TcpClient client(&loop, addr);
    client.SetConnectionCallback(onNewConnection);
    client.Connect();
    loop.Run();
}