// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License. 
// See accompanying files LICENSE.

#include <iostream>
#include <cstdint>

using namespace std;


void StartClient(std::string host, uint16_t port);
void StartServer(uint16_t port);

int main(int argc, char* argv[])
{
    uint16_t port = 8001;
    std::string host = "127.0.0.1";
    std::string mode = "server";
    if (argc >= 2)
    {
        mode = argv[1];
    }
    if (argc >= 3)
    {
        port = atoi(argv[2]);
    }
    if (argc >= 4)
    {
        host = argv[3];
    }

    if (mode == "client")
    {
        StartClient(host, port);
    }
    else
    {
        StartServer(port);
    }

    return 0;
}
