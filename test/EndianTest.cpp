/*
 * Copyright 2011-present Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// @author Tudor Bosman (tudorb@fb.com)

#include <gtest/gtest.h>
#include "base/Endian.h"
#include <random>

TEST(Bits, Endian_swap_uint)
{
    EXPECT_EQ(uint8_t(0xda), Endian::swap(uint8_t(0xda)));
    EXPECT_EQ(uint16_t(0x4175), Endian::swap(uint16_t(0x7541)));
    EXPECT_EQ(uint32_t(0x42efb918), Endian::swap(uint32_t(0x18b9ef42)));
    EXPECT_EQ(
        uint64_t(0xa244f5e862c71d8a), Endian::swap(uint64_t(0x8a1dc762e8f544a2)));
}

TEST(Bits, Endian_swap_real)
{
    std::mt19937_64 rng;
    auto f = std::uniform_real_distribution<float>()(rng);
    EXPECT_NE(f, Endian::swap(f));
    EXPECT_EQ(f, Endian::swap(Endian::swap(f)));
    auto d = std::uniform_real_distribution<double>()(rng);
    EXPECT_NE(d, Endian::swap(d));
    EXPECT_EQ(d, Endian::swap(Endian::swap(d)));
}
